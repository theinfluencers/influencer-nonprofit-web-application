import { Component, OnInit } from '@angular/core';
import {Apollo} from 'apollo-angular';
import {GETDATAFORINFLUENCERCARD, GETNONPROFITTYPEOFNONPROFITANDINFLUENCER, SEARCHQUERY} from '../../Apollo/queries';
import {HttpHeaders} from '@angular/common/http';
import {LocalstorageService} from '../../localstorage.service';
import {ActivatedRoute} from '@angular/router';
import {ApolloQueryResult} from 'apollo-client';

export class InfluencerCardQuery {
  data: {
    influencers: [{
      user: {
        id;
        influencer: {
          firstName,
          lastName,
          type_of_influencers: [{
            name: string
          }],
          googleAuthToken,
          googlePhotoUrl,
          };
        };
      }];
  };
}
export class InfluencerCard {

  user: {
    id;
    influencer: {
      firstName,
      lastName,
      type_of_influencers: [{
        name: string
      }],
      googleAuthToken,
      googlePhotoUrl,
    };
  };

}

class GetTypesOfNonProfitOrganisation {
  user: {
    non_profit: {
      type_of_non_profit_organisations: [{
        id
      }]
      type_of_influencers: [{
        id
      }]
    }
  };
}

@Component({
  selector: 'app-non-profit',
  templateUrl: './non-profit.component.html',
  styleUrls: ['./non-profit.component.scss']
})
export class NonProfitComponent implements OnInit {

  constructor(private apollo: Apollo,
              private localstorageService: LocalstorageService,
              private route: ActivatedRoute) { }
  influencers: InfluencerCard[];
  searchQuery = '';
  nonProfitTypesOfNonProfitOrganisation: GetTypesOfNonProfitOrganisation;
  ngOnInit(): void {
    this.influencers = [];
    // get the non profit's categories and send them in the search query
    this.apollo.query({
      query: GETNONPROFITTYPEOFNONPROFITANDINFLUENCER,
      variables: {
        userId: this.localstorageService.getId()
      },
      context: {
        headers: new HttpHeaders().set('Authorization', 'Bearer ' + this.localstorageService.getJwtToken()),
      }
    }).toPromise().then((gt: ApolloQueryResult<GetTypesOfNonProfitOrganisation>) => {
      const nonProfitTypes: string[] = [];
      const influencerTypes: string[] = [];
      gt.data.user.non_profit.type_of_non_profit_organisations.forEach((np: {id: string}) => {
        nonProfitTypes.push(np.id);
      });
      gt.data.user.non_profit.type_of_influencers.forEach((inf: {id: string}) => {
        influencerTypes.push(inf.id);
      });
      console.log(nonProfitTypes);
      this.route.queryParams.subscribe(params => {
          console.log(params); // {order: "popular"}
          if (params.q) {
            this.searchQuery = params.q;
            console.log(this.searchQuery); // popular
          } else {
            this.searchQuery = '';
          }
          this.influencers = [];
          this.apollo.mutate({
            mutation: SEARCHQUERY,
            variables: {
              text: this.searchQuery,
              nonProfit: nonProfitTypes,
              influencer: influencerTypes
            },
            context: {
              headers: new HttpHeaders().set('Authorization', 'Bearer ' + this.localstorageService.getJwtToken()),
            }
          }).toPromise().then((data: InfluencerCardQuery) => {
            //   this.influencers = [];
            //   data.data.influencers.forEach((val) => {
            //     console.log(val);
            //     this.influencers.push({
            //       id: val.user.id,
            //       firstName: val.user.influencer.firstName,
            //       lastName: val.user.influencer.lastName,
            //       typeOfNonProfits: Object.assign([], val.user.influencer.type_of_non_profit_organisations),
            //       googleAuthToken: val.user.influencer.googleAuthToken,
            //       googlePhotoUrl: val.user.influencer.googlePhotoUrl
            //     });
            //   });
            console.log(data);
            this.influencers = [...data.data.influencers];
          });
        });
    });

  }

}
