import { Component, OnInit } from '@angular/core';
import {AmazonLoginProvider, GoogleLoginProvider, SocialAuthService} from 'angularx-social-login';
import { SocialUser } from 'angularx-social-login';
import {Router} from '@angular/router';
import {LocalstorageService} from '../localstorage.service';
import {Apollo} from 'apollo-angular';
import {HttpHeaders} from '@angular/common/http';
import {user} from '../constants';
import {ToastrService} from 'ngx-toastr';
import {RegisterServiceService} from '../register/register-service.service';
import {DashboardService} from '../dashboard/dashboard.service';

export class SocialToken {
  google: {
    authToken
    photoUrl
  };
  isValid() {
    return this.google.authToken !== null;
  }
  constructor() {
    this.google = {
      authToken: null,
      photoUrl: null
    };
  }
}

@Component({
  selector: 'app-social-media',
  templateUrl: './social-media.component.html',
  styleUrls: ['./social-media.component.scss']
})
export class SocialMediaComponent implements OnInit {

  constructor(private authService: SocialAuthService,
              private router: Router,
              private localstorageService: LocalstorageService,
              private apollo: Apollo,
              private toastr: ToastrService,
              private registerServiceService: RegisterServiceService,
              private dashboardService: DashboardService
  ) {
    console.log('cons');
    this.token = new SocialToken();
  }
  user: SocialUser;
  loggedIn: boolean;
  token: SocialToken;

  // addSocialAuthTokesToDataBase() {
  //   user.photoUrl = this.token.google.photoUrl;
  //   this.apollo.mutate({
  //     mutation: ADDSOCIALAUTH,
  //     variables: {
  //       amazon: this.token.amazon.authToken,
  //       google: this.token.google.authToken,
  //       id: this.localstorageService.getId()
  //     },
  //     context: {
  //       headers: new HttpHeaders().set('Authorization', 'Bearer ' + this.localstorageService.getJwtToken()),
  //     }
  //   }).subscribe();
  // }
  ngOnInit() {
    console.log('onInit');
    this.authService.authState.subscribe((data: SocialUser) => {
      this.user = data;
      this.loggedIn = (user != null);
      console.log(user);
      if (this.loggedIn) {
        this.token.google.authToken = this.user.authToken;
        this.token.google.photoUrl = this.user.photoUrl;
        console.log('success message G');
        this.toastr.success('Google\'s Login success', 'Google Added');
      }
    });
  }
  onNext() {
    console.log(this.token);
    if (this.isValidToken()) {
      this.registerServiceService.submitCreateInfluencer(this.token);
      user.socialAuthToken = this.token;
    }
  }

  signInWithGoogle(): void {
    const googleLoginOptions = {
      scope: 'https://www.googleapis.com/auth/youtube.readonly ' +
        'https://www.googleapis.com/auth/yt-analytics-monetary.readonly ' +
        'https://www.googleapis.com/auth/yt-analytics.readonly'
    };
    this.authService.signIn(GoogleLoginProvider.PROVIDER_ID, googleLoginOptions);
  }

  // signInWithAmazon(): void {
  //   const amazonLoginOptions = {
  //     scope: 'profile profile:user_id postal_code'
  //   };
  //   this.authService.signIn(AmazonLoginProvider.PROVIDER_ID, amazonLoginOptions);
  // }

  isValidToken() {
    if (this.token === undefined) {
      return false;
    }
    return this.token.isValid();
  }
  // async checkExistingLoggedIn() {
  //   let isPresent = false;
  //   await this.apollo.query({
  //     query: CHECKSOCIALAUTH,
  //     variables: {
  //       id: this.localstorageService.getId()
  //     }
  //   }).subscribe((val: ApolloQueryResult<CheckSocialAuth>) => {
  //     isPresent = val.data.data.socialAuthTokensConnection.values.length > 0;
  //   });
  //   return isPresent;
  // }
}
